export interface FilterOptions {
  hasSunroof: Option
  isFourWheelDrive: Option
  hasLowMiles: Option
  hasPowerWindows: Option
  hasNavigation: Option
  hasHeatedSeats: Option
}

export interface Option {
  name: string
  display: string
  filterBy: boolean
}

export interface FilterOptionsBy {
  hasSunroof?: boolean
  isFourWheelDrive?: boolean
  hasLowMiles?: boolean
  hasPowerWindows?: boolean
  hasNavigation?: boolean
  hasHeatedSeats?: boolean
}

export const DefaultFilterOptions: Option[] = [
  { name: 'hasSunroof', display: 'Sun roof', filterBy: false },
  { name: 'isFourWheelDrive', display: '4 wheel drive', filterBy: false },
  { name: 'hasLowMiles', display: 'Low miles', filterBy: false },
  { name: 'hasPowerWindows', display: 'Power windows', filterBy: false },
  { name: 'hasNavigation', display: 'Navigation', filterBy: false },
  { name: 'hasHeatedSeats', display: 'Heated seats', filterBy: false }
]
