import { Component, OnInit, OnDestroy } from '@angular/core'
import { CarsService } from '../services/cars.service'
import { Car } from '../models/Car'
import { Subscription } from 'rxjs'
import { DefaultFilterOptions } from '../models/FilterOptions'
import { FormBuilder, FormGroup } from '../../../node_modules/@angular/forms'

@Component({
  selector: 'app-view-cars',
  templateUrl: './view-cars.component.html',
  styleUrls: ['./view-cars.component.scss']
})
export class ViewCarsComponent implements OnInit, OnDestroy {
  private sub: Subscription
  private filteredSub: Subscription
  cars: Car[]
  searching = false
  loading = false
  error = false
  filters = DefaultFilterOptions
  filterForm

  constructor(private carsService: CarsService, private fb: FormBuilder) {
    this.filterForm = this.fb.group({
      filters: this.buildFilters()
    })
  }

  ngOnInit() {
    this.loadCars()
  }

  ngOnDestroy() {
    this.sub.unsubscribe()
    if (this.filteredSub) this.filteredSub.unsubscribe()
  }

  buildFilters() {
    const arr = this.filters.map(opt => {
      return this.fb.control(opt.filterBy)
    })

    return this.fb.array(arr)
  }

  loadCars() {
    this.loading = true
    this.sub = this.carsService.setAllCarsSpoof().subscribe(
      data => {
        this.cars = data
        this.loading = false
      },
      error => {
        /*tslint:disable:no-console*/
        console.error(error)
        this.loading = false
        this.error = true
      }
    )
  }

  loadFilteredCars(filterBy: string[]) {
    this.filteredSub = this.carsService.filterCarsByFeature(filterBy).subscribe(
      data => {
        this.cars = data
        this.searching = false
      },
      error => {
        /*tslint:disable:no-console*/
        console.error(error)
        this.searching = false
        this.error = true
      }
    )
  }

  onSubmit({ value }: FormGroup) {
    this.searching = true
    const optionValues: string[] = this.filters.reduce((acc, _, i) => {
      if (value.filters[i]) {
        const optName = this.filters[i].name
        acc.push(optName)
      }
      return acc
    }, [])
    if (optionValues.length === 0) {
      this.loadCars()
    } else {
      this.loadFilteredCars(optionValues)
    }
  }
}
