import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { ViewCarsComponent } from '../view-cars/view-cars.component'

@NgModule({
  declarations: [ViewCarsComponent],
  imports: [BrowserModule, BrowserModule, FormsModule, ReactiveFormsModule],
  exports: [ViewCarsComponent],
  bootstrap: []
})
export class ViewCarsModule {}
