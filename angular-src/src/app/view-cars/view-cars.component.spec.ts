import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ViewCarsComponent } from './view-cars.component'
import { ReactiveFormsModule, FormsModule } from '../../../node_modules/@angular/forms'
import { CarsService } from '../services/cars.service'
import carsData from '../carsData'
import { By } from '@angular/platform-browser'

describe('ViewCarsComponent', () => {
  let component: ViewCarsComponent
  let fixture: ComponentFixture<ViewCarsComponent>
  let service: CarsService

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ViewCarsComponent],
      imports: [FormsModule, ReactiveFormsModule],
      providers: [CarsService]
    }).compileComponents()
    service = new CarsService()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCarsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should have loading set as false initially', () => {
    expect(component.loading).toBe(false, 'Not loading at first')
  })

  it('should load data on init', () => {
    spyOn(component, 'loadCars')
    component.ngOnInit()
    expect(component.cars.length).toBeGreaterThan(0)
    expect(component.loadCars).toHaveBeenCalled()
  })

  it('should change value of a checkbox', () => {
    const input = fixture.debugElement.query(By.css('#hasSunroof')).nativeElement

    input.click()
    fixture.detectChanges()

    expect(input.checked).toBeTruthy()
  })

  it('should change value of multiple checkboxes', () => {
    const input1 = fixture.debugElement.query(By.css('#hasSunroof')).nativeElement
    const input2 = fixture.debugElement.query(By.css('#hasLowMiles')).nativeElement

    input1.click()
    input2.click()
    fixture.detectChanges()

    expect(input1.checked).toBeTruthy()
    expect(input2.checked).toBeTruthy()
  })
})
