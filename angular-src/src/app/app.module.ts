import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AppComponent } from './app.component'

import { CarsService } from './services/cars.service'
import { ViewCarsModule } from './view-cars/view-cars.module'

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, BrowserModule, FormsModule, ReactiveFormsModule, ViewCarsModule],
  providers: [CarsService],
  bootstrap: [AppComponent]
})
export class AppModule {}
