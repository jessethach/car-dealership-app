import { TestBed } from '@angular/core/testing'

import { CarsService } from './cars.service'
import carsData from '../carsData'
import { Car } from '../models/Car'

describe('CarsService', () => {
  let service: CarsService
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CarsService]
    })
    service = new CarsService()
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })

  it('retrieves all the cars', () => {
    service.setAllCarsSpoof().subscribe(result => expect(result.length).toBeGreaterThan(0))
  })

  it('setAllCarsSpoof should return value from observable', (done: DoneFn) => {
    service.setAllCarsSpoof().subscribe(value => {
      expect(value).toBe(carsData)
      done()
    })
  })

  it('filterCarsByFeature should return filtered data', (done: DoneFn) => {
    const filterBy = ['hasSunroof', 'isFourWheelDrive']
    const expectedResult: Car[] = [
      {
        _id: '59d2698c05889e0b23959106',
        make: 'Toyota',
        year: 2012,
        color: 'Silver',
        price: 18696,
        hasSunroof: true,
        isFourWheelDrive: true,
        hasLowMiles: false,
        hasPowerWindows: true,
        hasNavigation: false,
        hasHeatedSeats: true
      }
    ]
    service.filterCarsByFeature(filterBy).subscribe(value => {
      expect(value[0]._id).toBe(expectedResult[0]._id)
      done()
    })
  })
})
