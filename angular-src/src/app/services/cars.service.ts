import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { Car } from '../models/Car'
import carsData from '../carsData'

@Injectable({
  providedIn: 'root'
})
export class CarsService {
  constructor() {}

  // Method to spoof car data from json file
  setAllCarsSpoof(): Observable<Car[]> {
    return new Observable(observer => {
      observer.next(carsData)
      observer.complete()
    })
  }

  // Method to spoof filtering logic from an API
  filterCarsByFeature(filterBy: string[]): Observable<Car[]> {
    return new Observable(observer => {
      const filteredCarData = filterBy.reduce((acc, key) => {
        return acc.filter(car => car[key])
      }, carsData)
      observer.next(filteredCarData)
      observer.complete()
    })
  }
}
