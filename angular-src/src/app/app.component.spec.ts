import { TestBed, async } from '@angular/core/testing'
import { AppComponent } from './app.component'
import { ViewCarsComponent } from './view-cars/view-cars.component'
import { FormsModule, ReactiveFormsModule } from '../../node_modules/@angular/forms'
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent, ViewCarsComponent],
      imports: [FormsModule, ReactiveFormsModule]
    }).compileComponents()
  }))
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent)
    const app = fixture.debugElement.componentInstance
    expect(app).toBeTruthy()
  }))
  it(`should have as title 'Bash Dealership'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent)
    const app = fixture.debugElement.componentInstance
    expect(app.title).toEqual('Bash Dealership')
  }))
  it('should render title in a nav', async(() => {
    const fixture = TestBed.createComponent(AppComponent)
    fixture.detectChanges()
    const compiled = fixture.debugElement.nativeElement
    expect(compiled.querySelector('nav').textContent).toContain('Bash Dealership')
  }))
})
