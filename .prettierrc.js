module.exports = {
  printWidth: 100,
  semi: false,
  trailingComma: 'none',
  singleQuote: true,
  overrides: [
    {
      files: ['*.css', '*.sass', '*.scss'],
      options: {
        singleQuote: false,
      },
    },
  ],
}
