### Important notes

There are two directories that contain the front end app (where we will mainly live) and a server API that was initially created to serve the Car dealership data. I found that it would be easier for anyone using the app locally to just use mock data via json/localStorage. The backend API does work in theory, but I traded the functionaliy off to concentrate on the frontend.

To use the front end Angular app, you will need to clone the repo and step into the `angular-src` directory. Steps to follow below.

### Getting Started

- Ensure you have [Node](https://nodejs.org/en/)
- Clone this repo

  ```
  $ git clone https://jessethach@bitbucket.org/jessethach/car-dealership-app.git
  ```

* Step into the `angular-src` directory

  ```
  $ cd angular-src
  ```

* Install dependencies

  ```
  $ npm install
  ```

* Run the app locally

  ```
  $ ng serve
  ```

  Then open localhost:4200/ on browser

* Run tests

  ```
  $ npm test
  ```

##### Tech

- Angular/TypeScript/Node.js/Karma
- Bulma.io for CSS layout
- Angular CLI

#### MVP

- ~~Table of cars~~
- ~~Filter by feature~~

#### Stretch Goals

- Pagination
- Persist session into LocalStorage
- Add routes
- Wire up backend API
- Create mileage threshold input
