module.exports = {
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2015,
    ecmaFeatures: {
      experimentalObjectRestSpread: true
    }
  },
  env: {
    node: true,
    browser: true,
    es6: true,
    mocha: true
  },
  plugins: ['angular'],
  extends: ['eslint:recommended', 'prettier'],
  globals: {
    module: false,
    inject: false,
    document: false
  },
  rules: {
    'comma-dangle': ['error', 'always-multiline'],
    strict: 'error',
    'prefer-const': 'error',
    'no-unused-vars': ['error', { argsIgnorePattern: '^_' }],
    'no-var': 'error',
    'linebreak-style': ['error', 'unix'],
    'lines-between-class-members': ['error', 'always'],
    'no-useless-escape': 'off',
    'import/extensions': [
      'error',
      'always',
      { js: 'never', json: 'never', ts: 'never', tsx: 'never' }
    ],
    'node/exports-style': ['error', 'module.exports'],
    'node/prefer-global/buffer': ['error', 'always'],
    'node/prefer-global/console': ['error', 'always'],
    'node/prefer-global/process': ['error', 'always'],
    'node/prefer-global/url-search-params': ['error', 'always'],
    'node/prefer-global/url': ['error', 'always']
  }
}
