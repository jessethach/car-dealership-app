const mongoose = require('mongoose')

const CarsSchema = mongoose.Schema({
  make: String,
  year: String,
  color: String,
  price: Number,
  hasSunroof: Boolean,
  isFourWheelDrive: Boolean,
  hasLowMiles: Boolean,
  hasPowerWindows: Boolean,
  hasNavigation: Boolean,
  hasHeatedSeats: Boolean
})

const Cars = (module.exports = mongoose.model('Cars', CarsSchema))

module.exports.getAllCars = callback => {
  Cars.find(callback)
}

module.exports.addCar = (newCar, callback) => {
  newCar.save(callback)
}

module.exports.deleteCarById = (id, callback) => {
  let query = { _id: id }
  Cars.remove(query, callback)
}
