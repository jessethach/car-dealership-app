const chai = require('chai')
const chaiHttp = require('chai-http')
chai.use(chaiHttp)
const expect = chai.expect
const mongoose = require('mongoose')
const request = chai.request

describe('Cars API', () => {
  it('should be able to retrieve all cars', done => {
    request('localhost:3000')
      .get('/api/cars')
      .end((err, res) => {
        expect(err).to.eql(null)
        expect(res).to.have.status(200)
        expect(Array.isArray(res.body)).to.eql(true)
        done()
      })
  })
})
