const express = require('express')
const router = express.Router()
const cars = require('../models/car')

router.get('/', (req, res) => {
  cars.getAllCars((err, cars) => {
    if (err) {
      res.json({ success: false, message: `Failed to load all cars. Error: ${err}` })
    } else {
      res.write(JSON.stringify({ success: true, cars: cars }, null, 2))
      res.end()
    }
  })
})

router.post('/', (req, res, next) => {
  let newCar = new car({
    make: req.body.make
  })
  cars.addCar(newCar, (err, list) => {
    if (err) {
      res.json({ success: false, message: `Failed to create a new car. Error: ${err}` })
    } else res.json({ success: true, message: 'Added successfully.' })
  })
})

router.delete('/:id', (req, res, next) => {
  let id = req.params.id
  cars.deleteCarById(id, (err, car) => {
    if (err) {
      res.json({ success: false, message: `Failed to delete the car. Error: ${err}` })
    } else if (car) {
      res.json({ success: true, message: 'Deleted successfully' })
    } else res.json({ success: false })
  })
})

module.exports = router
